package org.example;

public class Vehicle {
    private Engine engine;
    protected String name;
    protected int speed;
    protected int accelerate;

    public Vehicle(String name, int speed, int accelerate) {
        this.name = name;
        this.speed = speed;
        this.accelerate = accelerate;
    }

    public Vehicle(String name, int speed) {
        this.name = name;
        this.speed = speed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getAccelerate() {
        return accelerate;
    }

    public void setAccelerate(int accelerate) {
        this.accelerate = accelerate;
    }

    public void speedUp() {
        this.speed += this.accelerate;
        System.out.printf(String.format("%s:speed up to %d km/h \n", this.name, this.speed));
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
        this.accelerate = engine.accelerate;
    }
}
