package org.example;

public class Driver {
    private Vehicle vehicle;
    public void drive(Vehicle vehicle){
        this.vehicle = vehicle;
        System.out.println("Driver drive: "+ vehicle.name);
    }

    public void speedUp(){
        vehicle.speedUp();
    }
}
