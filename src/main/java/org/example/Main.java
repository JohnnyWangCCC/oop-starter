package org.example;


public class Main {
    public static void main(String[] args) {
        Car coolCar = new Car("Cool Car",25);
        coolCar.setEngine(Engine.oil);
        TruckCar truckCar = new TruckCar("Big Truck",20);
        truckCar.setEngine(Engine.electric);
        coolCar.speedUp();
        truckCar.speedUp();
        Driver diver = new Driver();
        diver.drive(coolCar);
        diver.speedUp();
        diver.drive(truckCar);
        diver.speedUp();
    }
}